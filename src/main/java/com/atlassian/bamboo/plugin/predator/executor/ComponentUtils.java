package com.atlassian.bamboo.plugin.predator.executor;


import com.atlassian.spring.container.ContainerManager;

public class ComponentUtils
{
    @SuppressWarnings("unchecked")
    public static <T> T getComponentIfNull(final T component, final String componentKey)
    {
        if (component == null)
        {
            return (T) ContainerManager.getComponent(componentKey);
        }
        else
        {
            return component;
        }
    }

}