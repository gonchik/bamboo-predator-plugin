package com.atlassian.bamboo.plugin.predator.executor;

import java.io.IOException;
import java.util.concurrent.Callable;

public class ProcessWaiter implements Callable<Integer> {
    private String command;
    private Process process;
    private Runtime runtime;

    public ProcessWaiter(String command, Runtime runtime) {
        this.command = command;
        this.runtime = runtime;
    }

    public Integer call() throws InterruptedException, IOException {
        process = runtime.exec(command);
        process.waitFor();

        return Integer.valueOf(process.exitValue());
    }

    public void destroy() {
        if (null != process) {
            process.destroy();
        }
    }
}
